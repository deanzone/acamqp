%%%-------------------------------------------------------------------
%%% @author adean
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 14. Aug 2018 4:49 AM
%%%-------------------------------------------------------------------
-module(acamqp_connection).
-author("adean").

-behaviour(gen_server).

-include("rabbit_msg.hrl").

%% API
-export([start_link/1, ack/1, publish/2, publish/3, start_consume/1, start_consume/2]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {conn_name, consumer, auto_ack = false, config}).

-define(IF_TRUE(Cond, Action), case Cond of
                                 true -> Action;
                                 false -> []
                               end ).

start_consume(Consumer) ->
  gen_server:cast(?SERVER, {start_consume, Consumer, false}).

start_consume(Consumer, AutoAck) ->
  gen_server:cast(?SERVER, {start_consume, Consumer, AutoAck}).

publish(RoutingKey, Payload) ->
  gen_server:cast(?SERVER, {publish, RoutingKey, Payload}).

publish(Exchange, RoutingKey, Payload) ->
  gen_server:cast(?SERVER, {publish, Exchange, RoutingKey, Payload}).

ack(Tag) ->
  gen_server:cast(?SERVER, {ack, Tag}).

-spec start_link(ConnectionName :: atom()) -> _.
start_link(ConnectionName) ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [ConnectionName], []).

init([ConnName]) ->
  Configs = maps:from_list(application:get_env(acamqp, connections, [])),
  Config = maps:from_list(maps:get(ConnName, Configs)),
  gen_server:cast(self(), connect),
  {ok, #state{conn_name = ConnName, config = Config}}.

handle_call(_Request, _From, State) ->
  Reply = ok,
  {reply, Reply, State}.

handle_cast({start_consume, Consumer, AutoAck}, State = #state{conn_name = ConnName}) ->
  case whereis(Consumer) of
    Pid when is_pid(Pid) -> acamqp_rabbit_conn:consume(ConnName);
    undefined -> erlang:error("Consumer Not Live")
  end,
  {noreply, State#state{consumer = Consumer, auto_ack = AutoAck}};

handle_cast({publish, Exchange, RoutingKey, Payload}, State = #state{conn_name = ConnName}) ->
  acamqp_rabbit_conn:publish(ConnName, Exchange, RoutingKey, Payload),
  {noreply, State};

handle_cast({publish, RoutingKey, Payload}, State = #state{conn_name = ConnName, config = Config}) ->
  acamqp_rabbit_conn:publish(ConnName, maps:get(exchange, Config), RoutingKey, Payload),
  {noreply, State};

handle_cast({ack, Tag}, State = #state{conn_name = ConnName}) ->
  acamqp_rabbit_conn:ack(ConnName, Tag),
  {noreply, State};

handle_cast(connect, State = #state{conn_name = ConnName, config = Config}) ->
  lager:debug("Connecting... to channel"),
  acamqp_rabbit_conn:connect(ConnName, Config, self()),
  {noreply, State};

handle_cast(_Msg, State) ->
  {noreply, State}.



handle_info(Msg = #rabbit_msg{delivery_tag = DTag},
            State = #state{conn_name = ConnName, consumer = Consumer, auto_ack = AutoAck}) ->
  erlang:send(Consumer, Msg),
  ?IF_TRUE(AutoAck, acamqp_rabbit_conn:ack(ConnName, DTag)),
  {noreply, State};

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.