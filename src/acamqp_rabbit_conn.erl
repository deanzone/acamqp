-module(acamqp_rabbit_conn).

-include("amqp_client.hrl").
-include("rabbit_msg.hrl").

-behaviour(gen_server).


-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-export([connect/3, publish/4, consume/1, ack/2, terminate/1]).


-record(state, {chan, config, call_back}).


% <<======== API ==========
connect(Server, Config = #{}, CallBack) ->
  gen_server:call(Server, {connect, Config, CallBack}).

consume(Server) ->
  gen_server:cast(Server, start_consume).

ack(Server, DTag) ->
  gen_server:cast(Server, {ack, DTag}).

publish(Server, Exchange, RoutingKey, Payload) ->
  gen_server:cast(Server, {publish, Exchange, RoutingKey, Payload}).

terminate(Server) ->
  gen_server:stop(Server, normal, 10000).

% =========  API ========>>

start_link(Server) when is_atom(Server) ->
  gen_server:start_link({local, Server}, ?MODULE, [], []).

init([]) ->
  {ok, #state{}}.

handle_call({connect, Config, CallBack}, _From, _State) ->
  lager:info("Trying to connect to the Channel"),
  {ok, Channel} = i_connect(Config),
  lager:info("Connected to Channel"),
  {reply, {ok, started}, #state{chan = Channel, config = Config, call_back = CallBack}};

handle_call(_Request, _From, State) ->
  Reply = ok,
  {reply, Reply, State}.

handle_cast(start_consume, State = #state{chan = Chan, config = Cfg}) ->
  i_consume(Chan, Cfg, self()),
  {noreply, State};

handle_cast({ack, DTag}, State = #state{chan = Chan}) ->
  amqp_channel:cast(Chan, #'basic.ack'{delivery_tag = DTag}),
  {noreply, State};

handle_cast({publish, Exchange, RoutingKey, Payload}, State = #state{chan = Channel}) ->
  Publish = #'basic.publish'{exchange = Exchange, routing_key = RoutingKey},
  amqp_channel:cast(Channel, Publish, #amqp_msg{payload = Payload}),
  {noreply, State};

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(#'basic.consume_ok'{}, State) ->
  {noreply, State};

handle_info(#'basic.cancel_ok'{}, State) ->
  {noreply, State};

handle_info({#'basic.deliver'{consumer_tag = CTag, delivery_tag = DTag, redelivered = RDel,
                              exchange = _Exchg, routing_key = RKey},
                              #amqp_msg{payload = Content}},
            State = #state{call_back = CallBack}) ->
  erlang:send(CallBack, #rabbit_msg{ consumer_tag = CTag, delivery_tag = DTag, redelivered = RDel,
                           routing_key = RKey, payload = Content}),
  {noreply, State};

handle_info({'DOWN', _, _, Channel, _}, State = #state{chan = Channel, config = Config}) ->
  lager:warning("Rabbit Disconnected! ~p", [Channel]),
  NewChannel = reconnect_to_rabbit(Config, 10),
  % detect the death of a rabbit and reconnect.
  monitor(process, NewChannel),
  i_consume(NewChannel, Config, self()),
  {noreply, State#state{ chan = NewChannel}};

handle_info(Msg, State) ->
  lager:warning("Unhandled-Msg: ~p", [Msg]),
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

reconnect_to_rabbit(Config, Delay) ->
  lager:warning("Connecting in ~ps....",[Delay]),
  Self = self(),
  spawn(fun() -> controlled_reconnect(Config, Self) end),
  receive
    {ok, Channel} -> Channel
  after Delay * 1000 ->
    reconnect_to_rabbit(Config, Delay + 3)
  end.

get_conn_params(Config = #{}) ->
  #amqp_params_network{
      username = maps:get( username, Config, "guest"),
      password	= maps:get(password, Config, "guest"),
      virtual_host = maps:get(virtual_host, Config, "/"),
      host = maps:get(host, Config, "localhost"),
      port = maps:get(port, Config, 5672),
      heartbeat	= maps:get(heartbeat, Config, 10)
  }.


controlled_reconnect(Config, Sender) ->
  %receive
    %{connect, Sender} ->
  Sender ! i_connect(Config),
  lager:info("Connected....... and Channel sent!").
  %end.


i_connect(Config) ->
  application:ensure_started(amqp_client),
  ConfRec = get_conn_params(Config),
  lager:debug("Connecting to the host with ~p", [ConfRec]),
  {ok, Connection} = amqp_connection:start(ConfRec),
  lager:debug("Openning Channel"),
  {ok, Channel} = amqp_connection:open_channel(Connection),
  lager:debug("rabbit_conn: Channel Opened, ~p", [Channel]),
  case maps:get(queue, Config, []) of
    [] -> [];
    Queue ->
      amqp_channel:call(Channel, #'basic.qos'{prefetch_count = maps:get(prefetch_count, Config, 1000)}),
      lager:debug("checking for declare"),
      case maps:get(declare_queue, Config, false) of
        true ->
          Declare = #'queue.declare'{queue = Queue},
          #'queue.declare_ok'{} = amqp_channel:call(Channel, Declare),
          lager:debug("rabbit_conn: Queue Declared");
        false ->
          ok
      end,
      lager:debug("checking for bind"),
      case {maps:get(exchange, Config, []), maps:get(routing_key, Config, [])} of
        {Exchange, Queue} when Exchange == [] orelse Queue == [] ->
          lager:warning("rabbit_conn: Exchange: ~p, Queue: ~p not binding.. using existing bindings", [Exchange, Queue]);

        {Exchange, RoutingKey} ->
          Binding = #'queue.bind'{queue = Queue,
            exchange    = Exchange,
            routing_key = RoutingKey
          },
          lager:debug("rabbit_conn: Calling Channel Bind"),
          #'queue.bind_ok'{} = amqp_channel:call(Channel, Binding),
          lager:debug("rabbit_conn: Queue Bind complete")
      end
  end,
  lager:debug("rabbit_conn: Connection Complete"),
  {ok, Channel}.

i_consume(Channel, #{queue := Queue} = _Config, Consumer) ->
  lager:debug("Params ~p, ~p, ~p", [Channel, Queue, Consumer]),
  Sub = #'basic.consume'{queue = Queue},
  #'basic.consume_ok'{consumer_tag = Tag } =
        amqp_channel:subscribe(Channel, Sub, Consumer),
  lager:info("Consume started for the Queue ~s", [Queue]),
  {ok, Tag}.

